import uno
import unohelper
from com.sun.star.lang import XServiceName
from com.sun.star.lang import XServiceInfo

from net.elmau.zaz.EasyMacro import XEasyMacro
from lodebug import Debug
from lotools import Tools, Arrays, Dict, AppEmail, Requests
from lodocuments import LODocuments
from localc import LOCalc


ID_EXTENSION = 'net.elmau.zaz.EasyMacro'
SERVICE = ('net.elmau.zaz.EasyMacro',)


class EasyMacro(unohelper.Base, XServiceName, XServiceInfo, XEasyMacro,
    Debug,
    Tools,
    Arrays,
    Dict,
    AppEmail,
    Requests,
    LODocuments,
    LOCalc
    ):

    def __init__(self, ctx):
        self.ctx = ctx
        super().__init__()

    def getServiceName(self):
        return ID_EXTENSION

    def getImplementationName(self):
        return ID_EXTENSION

    def getSupportedServiceNames(self):
        return (ID_EXTENSION,)


g_ImplementationHelper = unohelper.ImplementationHelper()
g_ImplementationHelper.addImplementation(EasyMacro, ID_EXTENSION, SERVICE)
