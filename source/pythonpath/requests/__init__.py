#!/usr/bin/env python3

import os
import sys


def get_info_path(path):
    path, filename = os.path.split(path)
    name, extension = os.path.splitext(filename)
    return (path, filename, name, extension)


path, *_ = get_info_path(__file__)

sys.path.append(path)

from .requests import *

