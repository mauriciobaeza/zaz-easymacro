#!/usr/bin/env python3


from net.elmau.zaz.EasyMacro import XDebug
import easymacro as app


class Debug(XDebug):

    def __init__(self):
        pass

    def mri(self, obj):
        app.mri(obj)
        return

    def info(self, data):
        app.info(data)
        return

    def debug(self, info):
        app.debug(info)
        return

    def error(self, info):
        self.msgerr = str(info)
        app.error(info)
        return

    def msgbox(self, message):
        return app.msgbox(message)

    def save_log(self, path, data):
        app.save_log(path, data)
        return
